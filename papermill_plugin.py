import os, sys
import json
from pathlib2 import Path
import subprocess
from tempfile import NamedTemporaryFile

from evaluation_system.api import plugin, parameters
from evaluation_system.misc import config
from evaluation_system.model.file import DRSFile

import logging

class Papermill(plugin.PluginAbstract):

    __category__ = 'support'
    __tags__ = ['plotting',]
    tool_developer = {'name': 'Martin Bergemann', 'email':'bergemann@dkrz.de'}
    __short_description__ = 'Parametrize a given notebook'
    __long_description__ = '''Run a notebook in batch mode
                              This notebook applies a notebook in batch mode.'''
    __version__ = (0,0,1)
    __parameters__ = parameters.ParameterDictionary(

            parameters.File(name='notebook',
                            mandatory=True,
                            help="Select your notebook",
                            file_extension="ipynb",
                            ),
            parameters.SolrField(name='variable', facet='variable', mandatory=True,
                                 help="Variable name"),
            parameters.SolrField(name='project', default=None, facet='project',
                                 help="Project name (will only be passed to the notebook if you set this variable)"),
            parameters.SolrField(name='product', default=None, facet='product',
                                 help="Product name (will only be passed to the notebook if you set this variable)"),
            parameters.SolrField(name='experiment', default=None, facet='experiment',
                                 help="Experiment name (will only be passed to the notebook if you set this variable)"),
            parameters.SolrField(name='institute', default=None, facet='institute',
                                 help="Institute name (will only be passed to the notebook if you set this variable)"),
            parameters.SolrField(name='realm', default=None, facet='realm',
                                 help="Realm name (will only be passed to the notebook if you set this variable)"),
            parameters.SolrField(name='model', default=None, facet='model',
                                 help="Model name (will only be passed to the notebook if you set this variable)"),
            parameters.SolrField(name='time_frequency', default=None, facet='time_frequency',
                                 help="Model output frequency (will only be passed to the notebook if you set this variable)"),
            parameters.SolrField(name='ensemble', default=None, facet='ensemble',
                                 help="ensemble name (will only be passed to the notebook if you set this variable)"),
           )

    def runTool(self, config_dict=None):
        """Gather all information to run the jupyter notebook in batch mode."""
        args = {}

        out_dir = self._special_variables.substitute({"d": "$USER_OUTPUT_DIR"})
        out_dir = Path(out_dir["d"]) / str(self.rowid)
        config_dict["outdir"] = str(out_dir)
        out_dir.mkdir(exist_ok=True, parents=True)
        search_facets = ('project', 'product', 'experiment',
                         'institute', 'model', 'time_frequency',
                          'ensemble', 'variable')
        # The notebook that will be applied in batch mode
        # Solr search factes
        solr_search = {k:config_dict[k] for k in search_facets if config_dict[k]}
        # Path to python3 binary
        python_env = '/work/ch1187/regiklim-ces/freva/xarray/bin/python'
        # Path to the python3 part that executes the notebook in batch mode
        tool_path = Path(__file__).parent / 'run_notebook.py'
        args['nb_file'] = config_dict['notebook']
        args['files'] = list(DRSFile.solr_search(path_only=True, **solr_search))
        args['variable'] = config_dict['variable']
        args['outdir'] = config_dict['outdir']
        # Write configuration to temporary json file
        with NamedTemporaryFile(suffix='.json') as tf:
            with open(tf.name, 'w') as f:
                json.dump(args, f)
            cmd = '{} {} {}'.format(python_env, tool_path, tf.name)
            self.call(cmd)
        return self.prepareOutput(config_dict['outdir'])

