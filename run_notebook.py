import argparse
from getpass import getuser
import json
from pathlib import Path
import sys

import papermill as pm
from ipykernel.kernelspec import install as install_kernel

def parse_config(argv=None):
    """Construct command line argument parser."""

    argp = argparse.ArgumentParser
    ap = argp(prog='map_plotter',
              description="""Plot some maps of different grids""",
              formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    ap.add_argument('configfile', metavar='configfile', type=Path,
                    help='The configuration file.')
    args = ap.parse_args()
    with args.configfile.expanduser().absolute().open() as f:
        return json.load(f)



def main(cfg, kernel_name='freva'):
    
    logging.basicConfig(
            level=logging.INFO,
            format='%(levelname)s;%(message)s'
    nb_file = Path(cfg.pop('nb_file')).expanduser().absolute()
    out_dir = Path(cfg.pop('outdir'))
    out_path = out_dir / 'papermill.ipynb'
    kernel_dir = Path('~/.local/share/jupyter/kernels').expanduser()
    # Execute the notebook
    pm.execute_notebook(str(nb_file),
                        str(out_path),
                        cwd=str(out_dir),
                        parameters=cfg,
                        progress_bar=False,
                        log_output=True,
                        prepare_only=False,
                        stdout_file=sys.stdout,
                        stderr_file=sys.stderr
                        )
    print(f'Notebook was successfully created in {out_path}.')
    # Construct a link to the notebook on the jupyterhub
    link = f'https://jupyterhub.dkrz.de/user/{getuser()}/preset/notebooks/{out_path}'
    # Write that link into a html file
    with (out_dir / 'papermill_link.html').open('w') as f:
        f.write(f'''<html><body><h1>Click the link below to open the notebook:</h1>
                   <p><a href="{link}" target=_blank>{out_path}</a></p></body></html>''')


if __name__ == '__main__':

    cfg = main(parse_config(sys.argv))
